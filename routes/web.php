<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/threads', 'ThreadsController@index')->name('threads.index');
Route::get('/threads/create', 'ThreadsController@create')->name('threads.create')->middleware('auth');
Route::post('/threads', 'ThreadsController@store')->name('threads.store')->middleware('auth');
Route::post('/threads/{thread}/reply', 'RepliesController@store')->name('reply.store')->middleware('auth');

Route::get('/threads/{channel}', 'ChannelController@index')->name('channels.index');
Route::get('/threads/{channel}/{thread}', 'ThreadsController@show')->name('threads.show');



