@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create a new Thread</div>

                    <div class="panel-body">
                        <form action="{{ route('threads.store') }}" method="post">
                            {{ csrf_field() }}

                            <div class="form-group @if($errors->has('title'))has-error @endif">
                                <label for="title">Title</label>
                                <input type="text" id="title" name="title" class="form-control"
                                       value="{{ old('title') }}" required>
                                @if($errors->has('title'))
                                    <span class="help-block">
                                        <ul>
                                            @foreach ($errors->get('title') as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group @if($errors->has('body'))has-error @endif">
                                <label for="body">Body</label>
                                <textarea name="body" id="body" cols="30" rows="10"
                                          class="form-control" required>{{ old('body') }}</textarea>
                                @if($errors->has('body'))
                                    <span class="help-block">
                                        <ul>
                                            @foreach ($errors->get('body') as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group @if($errors->has('channel_id'))has-error @endif">
                                <label for="channel_id">Choose a channel</label>
                                <select name="channel_id" id="channel_id" class="form-control" required>
                                    <option value="">Select a Channel</option>
                                    {{-- $channels provided by view composer in AppServiceProvider --}}
                                    @foreach($channels as $channel)
                                        <option value="{{ $channel->id }}"
                                                @if(old('channel_id') == $channel->id)selected @endif>
                                            {{ $channel->name }}
                                        </option>
                                    @endforeach
                                </select>
                                @if($errors->has('channel_id'))
                                    <span class="help-block">
                                        <ul>
                                            @foreach ($errors->get('channel_id') as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </span>
                                @endif
                            </div>

                            <button type="submit" class="btn btn-primary pull-right">Post</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
