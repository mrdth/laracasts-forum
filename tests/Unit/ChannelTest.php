<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\Traits\HandlesChannels;
use Tests\Traits\HandlesThreads;

class ChannelTest extends TestCase
{
    use DatabaseMigrations;
    use HandlesChannels;
    use HandlesThreads;

    public function testChannelHasThreads()
    {
        $channel = $this->createTestChannel();
        $thread = $this->createTestThread(['channel_id' => $channel->id]);

        $this->assertTrue($channel->threads->contains($thread));
    }
}
