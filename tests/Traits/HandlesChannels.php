<?php
/**
 * Created by PhpStorm.
 * User: mrdth
 * Date: 22/06/18
 * Time: 18:04
 */

namespace Tests\Traits;


use App\Channel;

trait HandlesChannels
{
    protected function channelIndexRoute(Channel $channel): string
    {
        return route('channels.index', $channel);
    }

    protected function makeTestChannel($attributes = []): Channel
    {
        return factory('App\Channel')->make($attributes);
    }

    protected function createTestChannel($attributes = []): Channel
    {
        return factory('App\Channel')->create($attributes);
    }
}